/**
  PRL projekt 2.
  Norbert Durcansky - xdurca01@stud.fit.vutbr.cz
  21.3.2018
*/

#include <mpi.h>
#include <algorithm>
#include <stdio.h>

#define FILE_NAME "numbers"

using namespace std;

/**
 * Function loads byte file to array
 * @return size of array
 */
int loadFile(uint8_t **buffer) {
    FILE *file;
    file = fopen(FILE_NAME, "rb");
    fseek(file, 0, SEEK_END);
    int size = ftell(file);
    fseek(file, 0, SEEK_SET);
    *buffer = (uint8_t *) malloc(size);

    if (file) {
        fread(*buffer, size, 1, file);
    } else {
        cerr << "ERROR: Subor sa nepodari nacitat" << endl;
        exit(1);
    }

    return size;
}

/**
 *  Function initializes array for CPU
 * @param inputArray - unsorted array of numbers
 * @param cpuArray - array to be created
 * @return size of created CPU array
 */
int getInitialArray(uint8_t *inputArray, uint8_t **cpuArray, int inputArraySize, int cpuCount, int cpuId) {
    int splitFactor = inputArraySize / cpuCount;
    int splitRemainder = inputArraySize % cpuCount;
    int beginOffset = cpuId * splitFactor;
    int hasOneMore = cpuId < splitRemainder;
    /** divide numbers between processors (each processor max one more number from remainder) */
    int begin = beginOffset + (hasOneMore ? cpuId : splitRemainder);
    int end = beginOffset + splitFactor + (hasOneMore ? cpuId + 1 : splitRemainder);
    int cpuArraySize = end - begin;

    *cpuArray = (uint8_t *) malloc(end - begin);
    memcpy(*cpuArray, inputArray + begin, end - begin);

    return cpuArraySize;
}

/**
 * Function sorts array with optimal seq. algorithm t(n)=O(n*log n)
 * @param receivedArray - array to be sorted
 * @param arraySize - size of array
 */
void preProcessing(uint8_t *receivedArray, int arraySize) {
    /** Sort by sequential algorithm with complexity (n*log n) */
    sort(receivedArray, receivedArray + arraySize);
}

/**
 * Function merges two arrays
 */
void merge(uint8_t *array1, int v1Size, uint8_t *array2, int v2Size, uint8_t **sortedArray) {

    int i = 0, j = 0, step = 0;
    *sortedArray = (uint8_t *) malloc(v1Size + v2Size);

    while (i < v1Size && j < v2Size) {
        if (array1[i] < array2[j]) {
            (*sortedArray)[step++] = array1[i++];
        } else {
            (*sortedArray)[step++] = array2[j++];
        }
    }

    while (i < v1Size) {
        (*sortedArray)[step++] = array1[i++];
    }

    while (j < v2Size) {
        (*sortedArray)[step++] = array2[j++];
    }
    return;
}

/**
 * Function simulates receiver for merge-splitting algorithm
 * @param receivedArray - received array from left neighbor
 * @param cpuArray - CPU's array
 */
void mergeSplitReceiver(uint8_t *receivedArray, uint8_t **cpuArray, MPI_Status *status,
                        int cpuId, int maxArraySize, int cpuArraySize) {
    int receivedCount;
    /** CPU receives vector from left neighbor*/
    MPI_Recv(receivedArray, maxArraySize, MPI_CHAR, cpuId - 1, 0, MPI_COMM_WORLD, status);
    MPI_Get_count(status, MPI_CHAR, &receivedCount);
    /** Merge part - merge both arrays to one  */
    uint8_t *mergedArray;
    merge(receivedArray, receivedCount, *cpuArray, cpuArraySize, &mergedArray);
    int mergedArraySize = receivedCount + cpuArraySize;
    /** Split part  - send left part back to left neighbor */
    int rightHalfSize = mergedArraySize / 2 + mergedArraySize % 2;
    /** Receiver gets right part */
    memcpy(*cpuArray, mergedArray + rightHalfSize, mergedArraySize / 2);
    MPI_Send(mergedArray, rightHalfSize, MPI_CHAR, cpuId - 1, 0, MPI_COMM_WORLD);
    free(mergedArray);
}

/**
 * Function simulates sender for merge-splitting algorithm
 * @param receivedArray - received array from right neighbor
 * @param cpuArray - CPU's array
 */
void mergeSplitSender(uint8_t *receivedArray, uint8_t **cpuArray, MPI_Status *status,
                      int cpuId, int maxArraySize, int cpuArraySize) {
    int receivedCount;
    /** Send array to right neighbor and receive new */
    MPI_Send(*cpuArray, cpuArraySize, MPI_CHAR, cpuId + 1, 0, MPI_COMM_WORLD);
    MPI_Recv(receivedArray, maxArraySize, MPI_CHAR, cpuId + 1, 0, MPI_COMM_WORLD, status);
    MPI_Get_count(status, MPI_CHAR, &receivedCount);
    memcpy(*cpuArray, receivedArray, receivedCount);
}

/**
 * Function prints formatted output
 * @param hasSpace - Number separator is " " or "\n"
 */
void getFormattedOutput(uint8_t *array, int arraySize, bool hasSpace) {

    char separator = hasSpace ? ' ' : '\n';

    for (int i = 0; i < arraySize; i++) {
        if (i != 0) {
            printf("%c", separator);
        }
        printf("%d", array[i]);
    }
    printf("\n");
}

/** Main function */
int main(int argc, char *argv[]) {

    int cpuCount;
    int cpuId;
    MPI_Status status;
    uint8_t *inputArray;

    int inputArraySize = loadFile(&inputArray);
    /** output sorted array of numbers */
    uint8_t *sortedArray = (uint8_t *) malloc(inputArraySize);

    /** MPI INIT */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &cpuCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &cpuId);
    uint8_t *cpuArray;
    // get vector of numbers for specific process and it's size
    int cpuArraySize = getInitialArray(inputArray, &cpuArray, inputArraySize, cpuCount, cpuId);
    int maxActiveCpu = (cpuCount > inputArraySize ? inputArraySize : cpuCount);
    bool hasRightNeighbor = cpuId + 1 < maxActiveCpu;
    bool hasLeftNeighbor = cpuId - 1 >= 0 && cpuId < inputArraySize;
    int maxCpuArraySize = cpuArraySize + 1;
    uint8_t receivedArray[maxCpuArraySize];
    /** Algorithm steps  = P/2 or numbersCount/2 + remainder */
    int steps = maxActiveCpu / 2 + ((maxActiveCpu % 2) > 0 ? 1 : 0);

    /** print unsorted numbers */
    if (cpuId == 0) {
        getFormattedOutput(inputArray, inputArraySize, true);
    }
/** Implemented Merge-splitting sort algorithm */
    preProcessing(cpuArray, cpuArraySize);

    for (int step = 1; step <= steps; step++) {
        /** 1. odd CPU */
        if (cpuId % 2 == 0) {
            if (hasRightNeighbor) {
                mergeSplitSender(receivedArray, &cpuArray, &status, cpuId, maxCpuArraySize,
                                 cpuArraySize);
            }
        } else {
            if (hasLeftNeighbor) {
                mergeSplitReceiver(receivedArray, &cpuArray, &status, cpuId, maxCpuArraySize,
                                   cpuArraySize);
            }
        }
        /** 2. even CPU */
        if (cpuId % 2 == 1) {
            if (hasRightNeighbor) {
                mergeSplitSender(receivedArray, &cpuArray, &status, cpuId, maxCpuArraySize,
                                 cpuArraySize);
            }
        } else {
            if (hasLeftNeighbor) {
                mergeSplitReceiver(receivedArray, &cpuArray, &status, cpuId, maxCpuArraySize,
                                   cpuArraySize);
            }
        }
    }
/** End of algorithm */

    /** Collect data from all CPUs */
    if (cpuId == 0) {
        int receivedCount; // number of bytes actually received from neighbor
        for (int number = 0, step = 0; number < cpuArraySize; number++) {
            sortedArray[step++] = cpuArray[number];
        }
        for (int index = 1, step = cpuArraySize; index < maxActiveCpu; index++) {
            MPI_Recv(receivedArray, maxCpuArraySize, MPI_CHAR, index, 0, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_CHAR, &receivedCount);

            for (int number = 0; number < receivedCount; number++) {
                sortedArray[step++] = receivedArray[number];
            }
        }
        getFormattedOutput(sortedArray, inputArraySize, false);
    } else {
        /** send sorted array for collection to root CPU (0) */
        MPI_Send(cpuArray, cpuArraySize, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }

    free(cpuArray);
    free(sortedArray);
    free(inputArray);

    MPI_Finalize();
    return 0;
}
