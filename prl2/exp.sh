#!/bin/bash

#parametre vstupu 
numbers=$1
cpus=$2

#vyrobeni souboru s random cisly
dd if=/dev/urandom bs=1 count=$numbers of=numbers

#preklad programu a spusteni
mpic++ --prefix /usr/local/share/OpenMPI -o mss mss.cpp -std=c++0x
mpirun --prefix /usr/local/share/OpenMPI -np $cpus mss

#vymazanie binarky a suboru
rm -f mss numbers
