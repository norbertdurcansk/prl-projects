#!/bin/bash

if [ $# -lt 2 ];then
    (>&2 echo "test.sh <numbers> <processors>")
    exit;
fi;

#parametre vstupu 
numbers=$1
cpus=$2

#vyrobeni souboru s random cisly
dd if=/dev/random bs=1 count=$numbers of=numbers > /dev/null 2>&1

#preklad programu a spusteni
mpic++ --prefix /usr/local/share/OpenMPI -o mss mss.cpp -std=c++0x
mpirun --prefix /usr/local/share/OpenMPI -np $cpus mss

#vymazanie binarky a suboru
rm -f mss numbers
