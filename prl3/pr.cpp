/**
  PRL projekt 3.
  Norbert Durcansky - xdurca01@stud.fit.vutbr.cz
  14.4.2018
*/

#include <mpi.h>
#include <stdio.h>
#include <math.h>

using namespace std;

/** Structure represents one edge */
typedef struct Edge {
    int nodeStart;
    int nodeEnd;
    int firstEdge; // index of the first edge in array
    int nextEdge = 0; // index of the next edge in array
    int reversedEdge; // index of the reversed edge
    bool isSet = false; // edge is set
} Edge;

/**
 * Function sets structure props
 */
void setEdge(Edge *edge, int nodeStart, int nodeEnd, bool isSet, int firstEdge, int nextEdge) {
    edge->nodeStart = nodeStart;
    edge->nodeEnd = nodeEnd;
    edge->isSet = isSet;
    edge->firstEdge = firstEdge;
    edge->nextEdge = nextEdge;
}

/**
 * Function creates edge to parent node
 */
void setEdgeParent(Edge **edges, int *index, int nodeStart, int nodeEnd) {
    if ((*edges)[*index].isSet) {
        // add new edge to array
        (*edges)[*index].nextEdge = *index + 1;
        (*index)++;
        // propagate first edge index to edges in array
        (*edges)[*index].firstEdge = (*edges)[*index - 1].firstEdge;
    } else {
        //array has just one edge
        (*edges)[*index].firstEdge = *index;
    }
    (*edges)[*index].nodeStart = nodeStart;
    (*edges)[*index].nodeEnd = nodeEnd;
}

/**
 * Function sets indexes to reversed edges
*/
void setReversedEdges(Edge **edges, int edgesSize) {
    for (int i = 0; i < edgesSize; i++) {
        for (int j = 0; j < edgesSize; j++) {
            if (i != j && (*edges)[i].nodeStart == (*edges)[j].nodeEnd &&
                (*edges)[i].nodeEnd == (*edges)[j].nodeStart) {
                (*edges)[i].reversedEdge = j;
                (*edges)[j].reversedEdge = i;
            }
        }
    }
}

/**
 * Function creates adjacency list represented as array of Edge structure
 * @param edges  - Newly created edges
 * @param initialTree  - String from stdin
 * @param edgesSize  - 2*n-2
 */
void createAdjacencyList(Edge **edges, char *initialTree, int edgesSize) {
    int initialTreeSize = strlen(initialTree);

    for (int i = 0, j = 0; i < initialTreeSize; i++) {
        int leftNodeIndex = (2 * (i + 1)) - 1;
        int rightNodeIndex = (2 * (i + 1));
        bool hasLeftNode = leftNodeIndex < initialTreeSize;
        bool hasRightNode = rightNodeIndex < initialTreeSize;
        bool isLeftChild = (i + 1) % 2 == 0 && (((i + 1) / 2) - 1 >= 0);
        bool isRightChild = i % 2 == 0 && ((i / 2) - 1 >= 0);

        if (hasLeftNode) {
            // create new edge from node to it's left child
            setEdge(&(*edges)[j], i, leftNodeIndex, true, j, hasRightNode ? j + 1 : 0);
            if (hasRightNode) {
                j++;
                // create new edge from node to it's right child
                setEdge(&(*edges)[j], i, rightNodeIndex, true, (*edges)[j - 1].firstEdge, 0);
            }
        }
        if (isLeftChild) {
            // create new edge from node to it's parent
            setEdgeParent(edges, &j, i, ((i + 1) / 2) - 1);

        } else if (isRightChild) {
            // create new edge from node to it's parent
            setEdgeParent(edges, &j, i, (i / 2) - 1);
        }
        j++;
    }
    // add reverse index for each edge
    setReversedEdges(edges, edgesSize);
    return;
}

/**
 * Function gathers values and broadcasts updated array
 * Used for Suffix-sum where just recv/send is allowed
 */
void sendReceive(int *value, int size, int *array, int cpuId, int cpuCount) {
    MPI_Status status;

    if (cpuId != 0) {
        MPI_Send(value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Recv(array, size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    } else {
        int receivedValue;
        array[0] = *value;
        for (int cpu = 1; cpu < cpuCount; cpu++) {
            MPI_Recv(&receivedValue, 1, MPI_INT, cpu, 0, MPI_COMM_WORLD, &status);
            array[cpu] = receivedValue;
        }
        for (int cpu = 1; cpu < cpuCount; cpu++) {
            MPI_Send(array, size, MPI_INT, cpu, 0, MPI_COMM_WORLD);
        }
    }
}

/**
 * Function creates eTour, algorithm from slides
 */
void createEtour(Edge *edges, int *eTour, int edgesSize, int cpuId) {
    // next index of edge is not null
    bool hasNextReversed = edges[(edges[cpuId].reversedEdge)].nextEdge != 0;
    int localEtour;

    /** eTour Algorithm */
    if (hasNextReversed) {
        localEtour = edges[(edges[cpuId].reversedEdge)].nextEdge;
    } else {
        // get first from adjList
        localEtour = (edges[edges[cpuId].reversedEdge].firstEdge);
    }
    /** Cut the root */
    if (localEtour == 0) {
        localEtour = cpuId;
    }
    MPI_Allgather(&localEtour, 1, MPI_INT, eTour, 1, MPI_INT, MPI_COMM_WORLD);
}

/**
 * Function returns list ranking, algorithm from slides
 */
void getListRanking(int *rank, int *succ, int edgesSize, int *eTour, int cpuId) {
    int localRank;
    // Succ array init
    int localSucc = eTour[cpuId];
    // Rank array init
    if (eTour[cpuId] == cpuId) {
        localRank = 0;
    } else {
        localRank = 1;
    }
    MPI_Allgather(&localRank, 1, MPI_INT, rank, 1, MPI_INT, MPI_COMM_WORLD);
    MPI_Allgather(&localSucc, 1, MPI_INT, succ, 1, MPI_INT, MPI_COMM_WORLD);
    // loop from slides
    for (int k = 0; k < log2(edgesSize); k++) {
        localRank = localRank + rank[localSucc];
        localSucc = succ[localSucc];
        MPI_Allgather(&localRank, 1, MPI_INT, rank, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Allgather(&localSucc, 1, MPI_INT, succ, 1, MPI_INT, MPI_COMM_WORLD);
    }
    /** Fix the rank and broadcast*/
    localRank = edgesSize - localRank;
    MPI_Allgather(&localRank, 1, MPI_INT, rank, 1, MPI_INT, MPI_COMM_WORLD);
}

/**
 * Function sets init weight for each edge, algorithm from slides
 */
void getWeight(int *rank, Edge *edges, int edgesSize, int *weight, int cpuId) {
    bool isForwardEdge = rank[cpuId] < rank[edges[cpuId].reversedEdge];
    int localWeight = isForwardEdge ? 1 : 0;
    MPI_Allgather(&localWeight, 1, MPI_INT, weight, 1, MPI_INT, MPI_COMM_WORLD);
}

/**
 * Function prints pre-order on stdout, algorithm from slides
 */
void getPreOrder(int *rank, Edge *edges, char *initialTree, int *weight, int cpuId, int cpuCount) {
    bool isForwardEdge = rank[cpuId] < rank[edges[cpuId].reversedEdge];
    int initialTreeSize = strlen(initialTree);
    MPI_Status status;
    /** Fix pre-order */
    int localPreorder = isForwardEdge ? (initialTreeSize - weight[cpuId] + 1) : -1;
    char preorder[initialTreeSize + 1];
    preorder[initialTreeSize] = '\0';

    /** Send pre-order to root if forward edge*/
    if (weight[cpuId] != 0 && cpuId != 0) {
        MPI_Send(&localPreorder, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
    /** Receive pre-order from each forward edge */
    if (cpuId == 0) {
        // First is root
        preorder[0] = initialTree[0];
        // Second is from root processor
        if (initialTreeSize > 1) {
            preorder[1] = initialTree[edges[0].nodeEnd];
        }
        for (int i = 1; i < cpuCount; i++) {
            int receivedPreOrder;
            if (weight[i] != 0) {
                MPI_Recv(&receivedPreOrder, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &status);
                preorder[receivedPreOrder - 1] = initialTree[edges[i].nodeEnd];
            }
        }
        /** print pre-order on stdout */
        printf("%s\n", preorder);
    }
}

/**
 * Function returns Suffix-sum, algorithm from slides
 */
void getSuffixSum(int *eTour, int *succ, int *weight, int cpuId, int edgesSize, int cpuCount) {
    // init succ
    int localSucc = eTour[cpuId];
    int localSuffixSum = localSucc == cpuId ? 0 : weight[cpuId];

    sendReceive(&localSuffixSum, edgesSize, weight, cpuId, cpuCount);
    sendReceive(&localSucc, edgesSize, succ, cpuId, cpuCount);

    for (int k = 0; k < log2(edgesSize); k++) {
        localSuffixSum = localSuffixSum + weight[localSucc];
        localSucc = succ[localSucc];
        sendReceive(&localSucc, edgesSize, succ, cpuId, cpuCount);
        sendReceive(&localSuffixSum, edgesSize, weight, cpuId, cpuCount);
    }
}

/** Main function */
int main(int argc, char *argv[]) {
    char *initialTree = argv[1];
    int initialTreeSize = strlen(initialTree);
    int cpuCount;
    int cpuId;
    int edgesSize = (2 * initialTreeSize - 2); // 2*n-2
    Edge *edges = (Edge *) malloc(edgesSize * sizeof(Edge));

    /** Global Etour */
    int *eTour = (int *) malloc(edgesSize * sizeof(int));
    /** Global Rank */
    int *rank = (int *) malloc(edgesSize * sizeof(int));
    /** Global Succ */
    int *succ = (int *) malloc(edgesSize * sizeof(int));
    /** Global Weight */
    int *weight = (int *) malloc(edgesSize * sizeof(int));

    createAdjacencyList(&edges, initialTree, edgesSize);
    /** MPI INIT */
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &cpuCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &cpuId);

    createEtour(edges, eTour, edgesSize, cpuId);
    getListRanking(rank, succ, edgesSize, eTour, cpuId);

/** Start of pre-order algorithm */

    getWeight(rank, edges, edgesSize, weight, cpuId);   // Step 1
    getSuffixSum(eTour, succ, weight, cpuId, edgesSize, cpuCount);  // Step 2
    getPreOrder(rank, edges, initialTree, weight, cpuId, cpuCount); // Step 3

/** End of pre-order algorithm */

    free(eTour);
    free(rank);
    free(succ);
    free(weight);
    free(edges);
    MPI_Finalize();

    return 0;
}