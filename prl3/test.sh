#!/bin/bash

if [ $# -lt 1 ];then
    (>&2 echo "test.sh <tree>")
    exit;
fi;

#parametre vstupu 
tree=$1
cpus=$((2*${#tree}-2))

#preklad programu a spusteni
mpic++ --prefix /usr/local/share/OpenMPI -o pr pr.cpp -std=c++0x
mpirun --prefix /usr/local/share/OpenMPI -np $cpus  ./pr $tree

#vymazanie binarky
rm -f pr